package com.wirecard;

import com.wirecard.domain.am.Simple;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

public class Main {

  public static void main(String[] args) throws Exception {
    
    ActorSystem system = ActorSystem.create();
    
    ActorRef simple = system.actorOf(Simple.props());
    
    simple.tell("dummy", ActorRef.noSender());
    simple.tell("other", ActorRef.noSender());
    simple.tell(Simple.NewMessage.INSTANCE, ActorRef.noSender());
  }
}
