package com.wirecard.domain.am;

import java.io.Serializable;

import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;

public class Simple extends AbstractActor {

  public static Props props() {
    return Props.create(Simple.class, Simple::new);
  }

  private Simple() {
    super();
  }

  @Override
  public Receive createReceive() {
    return ReceiveBuilder.create()
      .match(NewMessage.class, System.out::println)
      .match(String.class, "other"::equals, msg -> System.out.println("conditional handling"))
      .matchAny(msg -> System.out.println("actor received: " + msg.toString())).build();
  }

//protocol
  public static final class NewMessage implements Serializable {
    public static final NewMessage INSTANCE = new NewMessage();

    private NewMessage() {
    }
  }
}
